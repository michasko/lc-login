# LiveChat Login

## TODO
Use Stylus instead of pure CSS (CSS was used, because it was recommended).

## Development server
Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via Karma & Jasmine

## Author
Michał Leszczyk - michal.leszczyk.90@gmail.com
