import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
	let fixture: ComponentFixture<AppComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule
			],
			declarations: [
				AppComponent
			],
		});

		TestBed.compileComponents().then(() => {
			fixture = TestBed.createComponent(AppComponent);
		});
	}));

	const APP_TITLE = 'LiveChat Login';

	it('should create the app', async(() => {
		expect(fixture.debugElement.componentInstance).toBeTruthy();
	}));

	it(`should have as title 'app works!'`, async(() => {
		expect(fixture.debugElement.componentInstance.title).toEqual(APP_TITLE);
	}));

	it('should render title in a h1 tag', async(() => {
		fixture.detectChanges();
		const compiled: HTMLElement = fixture.debugElement.nativeElement;

		expect(compiled.querySelector('h1').textContent).toContain(APP_TITLE);
	}));
});
