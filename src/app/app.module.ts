import { NgModule } from '@angular/core';
import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { NgReduxRouter, NgReduxRouterModule, routerReducer } from '@angular-redux/router';
import { combineReducers, Reducer } from 'redux';
import { combineEpics, createEpicMiddleware, Epic } from 'redux-observable';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './containers/login/login.module';
import { AppState, PayloadAction, APP_INITIAL_STATE } from './store/store';

import { IsAuthenticatedGuard } from '~app/guards/is-authenticated/is-authenticated.guard';
import { IsNotAuthenticatedGuard } from '~app/guards/is-not-authenticated/is-not-authenticated.guard';
import { SessionEpics, sessionReducer } from '~app/store/session';
import { AuthService } from '~app/services/auth/auth.service';
import { SecretModule } from '~app/containers/secret/secret.module';
import { SharedModule } from '~app/shared.module';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		NgReduxModule,
		NgReduxRouterModule,
		SharedModule,
		LoginModule,
		SecretModule,
		AppRoutingModule
	],
	providers: [
		SessionEpics,
		AuthService,
		IsAuthenticatedGuard,
		IsNotAuthenticatedGuard
	],
	bootstrap: [ AppComponent ]
})
export class AppModule {

	constructor(
		ngRedux: NgRedux<AppState>,
		ngReduxRouter: NgReduxRouter,
		sessionEpics: SessionEpics
	) {
		const rootEpic: Epic<PayloadAction, NgRedux<AppState>> = combineEpics(
			sessionEpics.combinedEpics
		);
		const middleware: any[] = [ createEpicMiddleware(rootEpic) ];
		const reducers: Reducer<AppState> = combineReducers<AppState>({
			session: sessionReducer,
			router: routerReducer
		});

		ngRedux.configureStore(
			reducers,
			APP_INITIAL_STATE,
			middleware
		);

		// Enable syncing of Angular router state with Redux store
		ngReduxRouter.initialize();
	}
}
