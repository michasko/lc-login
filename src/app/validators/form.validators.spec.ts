import { FormValidators } from './form.validators';
import { AbstractControl, ValidatorFn } from '@angular/forms';

type Sample = {
	value: string;
	reason?: string;
	isValid: boolean;
}

describe('FormValidators', () => {
	function mockControl(value: string): AbstractControl {
		return <AbstractControl>{ value };
	}

	function runSpec(validator: ValidatorFn, validatorName: string, sample: Sample) {
		const ending: string = sample.isValid ? `for valid value of: ${sample.value}` : `when ${sample.reason}`;

		it(`should return ${String(sample.isValid)} ${ending}`, function () {
			const control: AbstractControl = mockControl(sample.value);

			if (sample.isValid) {
				expect(validator(control)).toBe(null);
			} else {
				expect(validator(control)).toEqual({ [validatorName]: true });
			}
		});
	}

	it('should implement method "passwordPolicy"', () => {
		expect(FormValidators.passwordPolicy).toEqual(jasmine.any(Function));
	});

	it('should implement method "email"', () => {
		expect(FormValidators.email).toEqual(jasmine.any(Function));
	});

	describe('#passwordPolicy validator', () => {
		const PASSWORD_SAMPLES: Sample[] = [
			{ value: 'Abcdef1', isValid: true },
			{ value: 'ABCdef123', isValid: true },
			{ value: 'ABCdef123@#@', isValid: true },
			{ value: 'abcd', isValid: false, reason: 'is too short' },
			{ value: 'abcdef', isValid: false, reason: 'contains no digits & capital letters' },
			{ value: 'abcde1', isValid: false, reason: 'contains no capital letters' },
			{ value: 'Abcdef', isValid: false, reason: 'contains no digits' },
			{ value: 'Abcdef$', isValid: false, reason: 'contains no digits' }
		];

		const validator: ValidatorFn = FormValidators.passwordPolicy();

		PASSWORD_SAMPLES.forEach((sample: Sample) => {
			runSpec(validator, 'passwordPolicy', sample);
		});
	});

	describe('#email validator', () => {
		const EMAIL_SAMPLES: Sample[] = [
			{
				value: 'something@something.com',
				isValid: true
			}, {
				value: '.wooly@example.com',
				isValid: true
			}, {
				value: 'someone@do-ma-in.com',
				isValid: true
			}, {
				value: 'someone@do.ma.in.com',
				isValid: true
			}, {
				value: 'with-dash@mail.com',
				isValid: true
			}, {
				value: 'a@test.pl',
				isValid: true
			}, {
				value: 'with+plus+sign+in+account+name@mail.com',
				isValid: true
			}, {
				value: 'a@b.b',
				isValid: false,
				reason: 'top-level domain is too short'
			}, {
				value: 'someone80@mail..com',
				isValid: false,
				reason: 'two dots appear one after another in domain name'
			}, {
				value: 'someone@@mail.com',
				isValid: false,
				reason: 'two @ characters appear one after another'
			}, {
				value: '@',
				isValid: false,
				reason: 'there is no account and no domain name'
			}, {
				value: '.',
				isValid: false,
				reason: 'there is no account and no domain name'
			}, {
				value: ' ',
				isValid: false,
				reason: 'there is no account and no domain name'
			}, {
				value: 'mail.com',
				isValid: false,
				reason: 'there is no account name'
			}, {
				value: 'with spaces@mail.com',
				isValid: false,
				reason: 'value contains spaces'
			}, {
				value: 'asdads@mail-.com',
				isValid: false,
				reason: 'domain name ends with dash'
			}, {
				value: 'asdasd@-.com',
				isValid: false,
				reason: 'domain name is a dash'
			}, {
				value: 'asdasd@mail+test.com',
				isValid: false,
				reason: 'domain name contains plus sign'
			}
		];

		const validator: ValidatorFn = FormValidators.email();

		EMAIL_SAMPLES.forEach((sample: Sample) => {
			runSpec(validator, 'email', sample);
		});
	});
});
