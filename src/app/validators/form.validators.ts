import { ValidatorFn, FormControl } from '@angular/forms';
import { PASSWORD_POLICY_GUARDS } from '~app/validators/password/password.guards';

export interface ValidationErrors {
	[key: string]: any;
}

export type ValidationGuard = (value: any) => boolean;

const VALID_EMAIL_REGEX: RegExp = /^([a-z0-9\.\-_\+])*([a-z0-9\-_\+])+@([a-z0-9]+[\.\-_]*)+([a-z0-9]+)+\.([a-z]{2,21})$/i;

export class FormValidators {
	static passwordPolicy(): ValidatorFn {
		return (control: FormControl): ValidationErrors|null => {
			const value: string = control.value;

			if (PASSWORD_POLICY_GUARDS.every((guard: ValidationGuard) => guard(value))) {
				return null;
			}

			return {
				passwordPolicy: true
			};
		};
	}

	static email(): ValidatorFn {
		return (control: FormControl): ValidationErrors | null => {
			if (VALID_EMAIL_REGEX.test(control.value)) {
				return null;
			}

			return {
				email: true
			};
		};
	}
}
