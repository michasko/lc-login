import {
	containsUpperCaseLetter,
	containsLowerCaseLetter,
	containsDigit,
	isLongEnough,
	PASSWORD_MINIMUM_LENGTH
} from '~app/validators/password/password.guards';

describe('Password Policy Guard', () => {
	describe('#containsUpperCaseLetter', () => {
		it('should return false for values without capital letters', () => {
			expect(containsUpperCaseLetter('asd')).toBe(false);
			expect(containsUpperCaseLetter('ąłćś')).toBe(false);
		});

		it('should return true for values with capital letters', () => {
			expect(containsUpperCaseLetter('Abc')).toBe(true);
			expect(containsUpperCaseLetter('abC')).toBe(true);
			expect(containsUpperCaseLetter('Ąbc')).toBe(true);
		});

	});

	describe('#containsLowerCaseLetter', () => {
		it('should return false for values with small letters', () => {
			expect(containsLowerCaseLetter('asd')).toBe(true);
			expect(containsLowerCaseLetter('ĄćŚ')).toBe(true);
			expect(containsLowerCaseLetter('ąłćś')).toBe(true);
		});

		it('should return false for values without small letters', () => {
			expect(containsLowerCaseLetter('ASD')).toBe(false);
			expect(containsLowerCaseLetter('AŁŚ')).toBe(false);
		});
	});

	describe('#containsDigit', () => {
		it('should return false for strings without digits', () => {
			expect(containsDigit('asdasd')).toBe(false);
			expect(containsDigit('asd!@#')).toBe(false);
		});

		it('should return true for strings with digits', () => {
			expect(containsDigit('asd23')).toBe(true);
			expect(containsDigit('asddsa1')).toBe(true);
			expect(containsDigit('asddsa1@#')).toBe(true);
		});
	});

	describe('#isLongEnough', () => {
		it(`should return false for strings with less than ${PASSWORD_MINIMUM_LENGTH} characters`, () => {
			expect(isLongEnough('a'.repeat(PASSWORD_MINIMUM_LENGTH - 1))).toBe(false);
		});

		it(`should return true for strings with at least ${PASSWORD_MINIMUM_LENGTH} characters`, () => {
			expect(isLongEnough('a'.repeat(PASSWORD_MINIMUM_LENGTH))).toBe(true);
			expect(isLongEnough('a'.repeat(PASSWORD_MINIMUM_LENGTH + 2))).toBe(true);
		});
	});
});
