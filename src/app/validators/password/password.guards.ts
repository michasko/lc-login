import { ValidationGuard } from '~app/validators/form.validators';

const DIGIT_REGEXP = /\d/;
export const PASSWORD_MINIMUM_LENGTH = 6;

/** Password policy guards */
export function containsUpperCaseLetter(value: string): boolean {
	return value.split('').some((letter: string) => letter.toLocaleLowerCase() !== letter);
}

export function containsLowerCaseLetter(value: string): boolean {
	return value.split('').some((letter: string) => letter.toLocaleUpperCase() !== letter);
}

export function containsDigit(value: string): boolean {
	return value.match(DIGIT_REGEXP) !== null;
}

export function isLongEnough(value: string): boolean {
	return value.length >= PASSWORD_MINIMUM_LENGTH;
}

export const PASSWORD_POLICY_GUARDS: ValidationGuard[] = [
	containsLowerCaseLetter,
	containsUpperCaseLetter,
	containsDigit,
	isLongEnough
];
