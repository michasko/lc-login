import { Injectable } from '@angular/core';
import { Credentials, User } from '~app/store/session/session.model';
import { Observable } from 'rxjs';

export interface AuthenticationError {
	message: string;
}

export interface AuthenticationSuccess {
	user: User;
}

export const VALID_CREDENTIALS: Credentials = {
	username: 'test@test.pl',
	password: 'Password1'
};

const RESPONSE_DELAY = 2000;

@Injectable()
export class AuthService {
	login(credentials: Credentials): Observable<AuthenticationSuccess | AuthenticationError> {
		/** Mocked results **/
		if (credentials.username === VALID_CREDENTIALS.username) {
			return Observable
				.of<AuthenticationSuccess>({
					user: {
						username: credentials.username
					}
				})
				.delay(RESPONSE_DELAY);
		}

		return Observable
			.throw({
				message: 'Invalid username or password!'
			})
			.materialize()
			.delay(RESPONSE_DELAY)
			.dematerialize<AuthenticationError>();
	}
}
