import { TestBed, inject } from '@angular/core/testing';
import { AuthService, VALID_CREDENTIALS, AuthenticationSuccess, AuthenticationError } from './auth.service';
import { Credentials } from '~app/store/session';

describe('AuthService', () => {
	const INVALID_CREDENTIALS: Credentials = {
		username: 'asdasd',
		password: 'asdsad'
	};

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ AuthService ]
		});
	});

	it('should authenticate user when valid credentials are provided',
		inject([ AuthService ], (service: AuthService) => {
			service.login(VALID_CREDENTIALS).subscribe((result: AuthenticationSuccess) => {
				expect(result).toEqual({
					user: {
						username: VALID_CREDENTIALS.username
					}
				});
			});
		})
	);

	it('should not authenticate user when valid credentials are provided',
		inject([ AuthService ], (service: AuthService) => {
			service.login(INVALID_CREDENTIALS).subscribe(() => {}, (result: AuthenticationError) => {
				expect(result).toEqual({
					message: 'Invalid username or password!'
				});
			});
		})
	);
});
