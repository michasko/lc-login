import { Session } from './session/session.model';
import { Action } from 'redux';
import { SESSION_INITIAL_STATE } from '~app/store/session';

export interface AppState {
	session: Session;
}

export interface PayloadAction extends Action {
	type: string;
	payload?: any;
}

export const APP_INITIAL_STATE: AppState = {
	session: SESSION_INITIAL_STATE
};
