import { NgRedux } from '@angular-redux/store';

import { SessionActions, Credentials } from './index';

describe('Session Actions', () => {
	const CREDENTIALS: Credentials = {
		username: 'EMAIL',
		password: 'PASSWORD'
	};

	let actions: SessionActions;
	let mockRedux: NgRedux<any>;

	beforeEach(() => {
		mockRedux = jasmine.createSpyObj<NgRedux<any>>('NgRedux', ['dispatch']);
		actions = new SessionActions(mockRedux);
	});

	describe('login action', () => {
		it('should dispatch session/login action', () => {
			actions.login(CREDENTIALS);

			expect(mockRedux.dispatch).toHaveBeenCalledWith({
				type: SessionActions.LOGIN,
				payload: CREDENTIALS
			});
		});
	});
});
