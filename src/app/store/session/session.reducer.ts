import { Session } from './session.model';
import { PayloadAction } from '../store';
import { SessionActions } from './session.actions';

export const SESSION_INITIAL_STATE: Session = {
	isAuthenticated: false,
	error: null,
	user: null
};

export function sessionReducer(state: Session = SESSION_INITIAL_STATE, action: PayloadAction): Session {
	switch (action.type) {
		case SessionActions.UNAUTHORIZED:
			return Object.assign({}, state, {
				error: null,
				isAuthenticated: false,
				user: null
			});

		case SessionActions.LOGIN_FAIL:
			return Object.assign({}, state, {
				error: action.payload,
				isAuthenticated: false,
				user: null
			});

		case SessionActions.LOGIN_SUCCESS:
			return Object.assign({}, state, {
				error: null,
				isAuthenticated: true,
				user: action.payload
			});

		case SessionActions.LOGIN:
			return Object.assign({}, state, {
				error: null,
				isAuthenticated: false,
				user: null
			});

		default:
			return Object.assign({}, state);
	}
}
