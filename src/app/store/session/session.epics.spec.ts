import { TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { PayloadAction } from '~app/store/store';
import { SessionEpics } from './session.epics';
import { SessionActions } from './session.actions';
import { AuthService } from '~app/services/auth/auth.service';

const LOGIN_ACTION_SUCCESS_REQUEST: PayloadAction = {
	type: SessionActions.LOGIN,
	payload: {
		username: 'test@test.pl',
		password: 'Password1'
	}
};

const LOGIN_ACTION_FAIL_REQUEST: PayloadAction = {
	type: SessionActions.LOGIN,
	payload: {
		username: 'test@test.com',
		password: 'Password123'
	}
};

const LOGIN_ACTION_SUCCESS_RESPONSE: PayloadAction = {
	type: SessionActions.LOGIN_SUCCESS,
	payload: {
		user: {
			username: 'test@test.pl'
		}
	}
};

const LOGIN_ACTION_FAIL_RESPONSE: PayloadAction = {
	type: SessionActions.LOGIN_FAIL,
	payload: 'Invalid username or password!'
};

describe('Session Epics', () => {
	const authSpy: AuthService = jasmine.createSpyObj<AuthService>('AuthService', ['login']);

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				SessionEpics,
				{
					provide: AuthService,
					useValue: authSpy
				}
			]
		});
	});

	it('should dispatch session/login-success action',
		inject([ SessionEpics ], (epics: SessionEpics) => {
			(<jasmine.Spy>authSpy.login).and.returnValue(Observable.of({
				user: {
					username: 'test@test.pl'
				}
			}));

			epics.login(Observable.of(LOGIN_ACTION_SUCCESS_REQUEST)).subscribe((action: PayloadAction) => {
				expect(action).toEqual(LOGIN_ACTION_SUCCESS_RESPONSE);
			});
		})
	);

	it('should dispatch session/login-fail action',
		inject([ SessionEpics ], (epics: SessionEpics) => {
			(<jasmine.Spy>authSpy.login).and.returnValue(Observable.throw({
				message: 'Invalid username or password!'
			}));

			epics.login(Observable.of(LOGIN_ACTION_FAIL_REQUEST)).subscribe((action: PayloadAction) => {
				expect(action).toEqual(LOGIN_ACTION_FAIL_RESPONSE);
			});
		})
	);
});
