import { SessionActions, sessionReducer, SESSION_INITIAL_STATE, Session } from './index';

describe('sessionReducer', () => {
	it('should set isAuthenticated to false and nullify error & user on session/unauthorized action', () => {
		const nextState: Session = sessionReducer(
			SESSION_INITIAL_STATE,
			{
				type: SessionActions.UNAUTHORIZED
			});

		expect(nextState.error).toEqual(null);
		expect(nextState.user).toEqual(null);
		expect(nextState.isAuthenticated).toBeFalsy();
	});

	it('should set isAuthenticated to false, set error message & nullify user on session/login-fail action', () => {
		const nextState: Session = sessionReducer(
			SESSION_INITIAL_STATE,
			{
				type: SessionActions.LOGIN_FAIL,
				payload: 'Invalid credentials'
			});

		expect(nextState.error).toEqual('Invalid credentials');
		expect(nextState.user).toEqual(null);
		expect(nextState.isAuthenticated).toBeFalsy();
	});

	it('should set isAuthenticated to false, set user & nullify error message on session/login-success action', () => {
		const nextState: Session = sessionReducer(
			SESSION_INITIAL_STATE,
			{
				type: SessionActions.LOGIN_SUCCESS,
				payload: {
					username: 'USER'
				}
			});

		expect(nextState.error).toEqual(null);
		expect(nextState.user).toEqual({ username: 'USER' });
		expect(nextState.isAuthenticated).toBeTruthy();
	});

	it('should set isAuthenticated to false and nullify error & user on session/login action', () => {
		const nextState: Session = sessionReducer(
			SESSION_INITIAL_STATE,
			{
				type: SessionActions.UNAUTHORIZED
			});

		expect(nextState.error).toEqual(null);
		expect(nextState.user).toEqual(null);
		expect(nextState.isAuthenticated).toBeFalsy();
	});
});
