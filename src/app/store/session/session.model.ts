export interface Session {
	isAuthenticated: boolean;
	error: string | null;
	user: User | null;
}

export interface Credentials {
	username: string;
	password: string;
	remember?: string;
}

export interface User {
	username: string;
}
