import { NgRedux } from '@angular-redux/store';
import { AppState } from '../store';
import { Credentials } from './session.model';
import { Injectable } from '@angular/core';

@Injectable()
export class SessionActions {
	static readonly UNAUTHORIZED: string = 'session/unauthorized';

	static readonly LOGIN: string = 'session/login';
	static readonly LOGIN_FAIL: string = 'session/login-fail';
	static readonly LOGIN_SUCCESS: string = 'session/login-success';

	constructor(
		private ngRedux: NgRedux<AppState>
	) {}

	login(credentials: Credentials): void {
		this.ngRedux.dispatch({
			type: SessionActions.LOGIN,
			payload: credentials
		});
	}
}
