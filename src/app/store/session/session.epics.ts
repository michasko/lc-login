import { Injectable } from '@angular/core';
import { AppState, PayloadAction } from '../store';
import { NgRedux } from '@angular-redux/store';
import { Epic, combineEpics } from 'redux-observable';

import { AuthService, AuthenticationError, AuthenticationSuccess } from '~app/services/auth/auth.service';
import { Observable } from 'rxjs';
import { SessionActions } from '~app/store/session/session.actions';

@Injectable()
export class SessionEpics {
	combinedEpics: Epic<PayloadAction, NgRedux<AppState>>;

	constructor(
		private authService: AuthService
	) {
		this.combinedEpics = combineEpics(
			this.login
		);
	}

	login = (action$: Observable<PayloadAction>): Observable<PayloadAction> => {
		return action$
			.filter((action: PayloadAction) => action.type === SessionActions.LOGIN)
			.mergeMap(({ payload }: PayloadAction) => {
				return this.authService.login(payload)
					.map((data: AuthenticationSuccess) => {
						return {
							type: SessionActions.LOGIN_SUCCESS,
							payload: Object.assign({}, data)
						};
					})
					.catch((error: AuthenticationError) => {
						return [{
							type: SessionActions.LOGIN_FAIL,
							payload: error.message
						}];
					});
			});
	};
}
