import { TestBed, inject } from '@angular/core/testing';
import { IsAuthenticatedGuard } from './is-authenticated.guard';
import { AppState } from '~app/store/store';
import { NgRedux } from '@angular-redux/store';
import { Router } from '@angular/router';

const APP_STATE: AppState = {
	session: {
		isAuthenticated: false,
		user: null,
		error: null
	}
};

describe('IsAuthenticatedGuard', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				IsAuthenticatedGuard,
				{
					provide: Router,
					useValue: jasmine.createSpyObj('Router', ['navigate'])
				},
				{
					provide: NgRedux,
					useValue: {
						getState: () => APP_STATE
					}
				}
			]
		});
	});

	it('should allow activating a route if user is authenticated',
		inject([ IsAuthenticatedGuard ], (guard: IsAuthenticatedGuard) => {
			APP_STATE.session.isAuthenticated = true;

			expect(guard.canActivate()).toBe(true);
		})
	);

	it('should NOT allow activating a route if user is NOT authenticated',
		inject([ IsAuthenticatedGuard ], (guard: IsAuthenticatedGuard) => {
			APP_STATE.session.isAuthenticated = false;

			expect(guard.canActivate()).toBe(false);
		})
	);

	it('should redirect to /login page if user is NOT authenticated',
		inject([ IsAuthenticatedGuard, Router ], (guard: IsAuthenticatedGuard, router: Router) => {
			APP_STATE.session.isAuthenticated = false;

			guard.canActivate();

			expect(router.navigate).toHaveBeenCalledWith(['/login']);
		})
	);
});
