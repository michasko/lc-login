import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '~app/store/store';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {
	constructor(
		private router: Router,
		private state: NgRedux<AppState>
	) {}

	canActivate(): boolean {
		const isAuthenticated: boolean = this.state.getState().session.isAuthenticated;

		if (!isAuthenticated) {
			this.router.navigate(['/login']);

			return false;
		}

		return true;
	}
}
