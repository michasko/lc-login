import { TestBed, inject } from '@angular/core/testing';
import { IsNotAuthenticatedGuard } from './is-not-authenticated.guard';
import { AppState } from '~app/store/store';
import { NgRedux } from '@angular-redux/store';

const APP_STATE: AppState = {
	session: {
		isAuthenticated: false,
		user: null,
		error: null
	}
};

describe('IsNotAuthenticatedGuard', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				IsNotAuthenticatedGuard,
				{
					provide: NgRedux,
					useValue: {
						getState: () => APP_STATE
					}
				}
			]
		});
	});

	it('should allow activating a route if user is authenticated',
		inject([ IsNotAuthenticatedGuard ], (guard: IsNotAuthenticatedGuard) => {
			APP_STATE.session.isAuthenticated = true;

			expect(guard.canActivate()).toBe(false);
		})
	);

	it('should NOT allow activating a route if user is NOT authenticated',
		inject([ IsNotAuthenticatedGuard ], (guard: IsNotAuthenticatedGuard) => {
			APP_STATE.session.isAuthenticated = false;

			expect(guard.canActivate()).toBe(true);
		})
	);
});
