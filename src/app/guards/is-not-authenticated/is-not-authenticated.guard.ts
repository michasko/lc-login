import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '~app/store/store';

@Injectable()
export class IsNotAuthenticatedGuard implements CanActivate {
	constructor(
		private state: NgRedux<AppState>
	) {}

	canActivate(): boolean {
		return !this.state.getState().session.isAuthenticated;
	}
}
