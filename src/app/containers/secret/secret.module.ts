import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SessionActions } from '~app/store/session';
import { SecretRoutingModule } from '~app/containers/secret/secret-routing.module';
import { SecretComponent } from '~app/containers/secret/secret.component';

@NgModule({
	declarations: [
		SecretComponent
	],
	imports: [
		BrowserModule,
		SecretRoutingModule
	],
	providers: [
		SessionActions
	],
	bootstrap: []
})
export class SecretModule { }
