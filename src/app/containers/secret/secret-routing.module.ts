import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsAuthenticatedGuard } from '~app/guards/is-authenticated/is-authenticated.guard';
import { SecretComponent } from '~app/containers/secret/secret.component';

const routes: Routes = [
	{
		path: 'secret',
		component: SecretComponent,
		canActivate: [ IsAuthenticatedGuard ]
	}
];

@NgModule({
	imports: [ RouterModule.forChild(routes) ],
	exports: [ RouterModule ],
	providers: []
})
export class SecretRoutingModule { }
