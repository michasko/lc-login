import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { SecretComponent } from './secret.component';

describe('SecretComponent', () => {
	let fixture: ComponentFixture<SecretComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				SecretComponent
			]
		});

		TestBed.compileComponents().then(() => {
			fixture = TestBed.createComponent(SecretComponent);
		});
	}));

	const SECRET_MESSAGE = 'Login successful';

	it('should render the secret message', async(() => {
		fixture.detectChanges();
		expect(fixture.debugElement.nativeElement.textContent).toContain(SECRET_MESSAGE);
	}));
});
