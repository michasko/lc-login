import { Component } from '@angular/core';

@Component({
	selector: 'lc-secret',
	templateUrl: 'secret.component.html',
	styleUrls: [ 'secret.component.css' ]
})
export class SecretComponent {
	message = 'Login successful';
}
