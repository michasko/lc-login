import { NgRedux } from '@angular-redux/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Session, SessionActions } from '~app/store/session';
import { AppState } from '~app/store/store';
import { LoginComponent } from './login.component';

const SUCCESS_SESSION: Session = {
	error: null,
	user: {
		username: 'abcd@abcd.pl'
	},
	isAuthenticated: true
};

const LOGIN_ERROR_SESSION: Session = {
	error: 'Invalid credentials',
	user: null,
	isAuthenticated: false
};

describe('Login Container', () => {
	let mockRouter: Router;
	let mockNgRedux: NgRedux<AppState>;
	let mockActions: SessionActions;
	let loginComponent: LoginComponent;
	let selectSpy: jasmine.Spy;
	let mockSessionSubscription: Subscription;

	beforeEach(() => {
		mockRouter = jasmine.createSpyObj<Router>('Router', ['navigate']);
		mockNgRedux = jasmine.createSpyObj<NgRedux<AppState>>('NgRedux', ['select']);
		mockActions = jasmine.createSpyObj<SessionActions>('SessionActions', ['errorClean', 'loginUser', 'facebookLogin']);
		mockSessionSubscription = jasmine.createSpyObj<Subscription>('sessionSubscription', ['unsubscribe']);

		selectSpy = (<jasmine.Spy>(mockNgRedux.select));

		loginComponent = new LoginComponent(mockNgRedux, mockActions, mockRouter);
		loginComponent.sessionSubscription = mockSessionSubscription;
	});

	it('should navigate to root container upon successful login', () => {
		selectSpy.and.returnValue(Observable.from([ SUCCESS_SESSION ]));
		loginComponent.ngOnInit();

		expect(mockRouter.navigate).toHaveBeenCalledWith(['/']);
	});

	it('should return error message upon unsuccessful login', () => {
		selectSpy.and.returnValue(Observable.from([ LOGIN_ERROR_SESSION ]));
		loginComponent.ngOnInit();

		expect(loginComponent.formErrorMessage).toBe(LOGIN_ERROR_SESSION.error);
	});

	it('should unsubscribe the session state Observable', () => {
		loginComponent.ngOnDestroy();

		expect(mockSessionSubscription.unsubscribe).toHaveBeenCalled();
	});
});
