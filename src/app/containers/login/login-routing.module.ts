import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { IsNotAuthenticatedGuard } from '~app/guards/is-not-authenticated/is-not-authenticated.guard';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent,
		canActivate: [ IsNotAuthenticatedGuard ]
	}
];

@NgModule({
	imports: [ RouterModule.forChild(routes) ],
	exports: [ RouterModule ],
	providers: []
})
export class LoginRoutingModule { }
