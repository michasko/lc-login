import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SessionActions } from '~app/store/session';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoginBoxComponent } from './components/login-box/login-box.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '~app/shared.module';

@NgModule({
	declarations: [
		LoginComponent,
		LoginBoxComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		SharedModule,
		LoginRoutingModule
	],
	providers: [
		SessionActions
	],
	bootstrap: []
})
export class LoginModule { }
