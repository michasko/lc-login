import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionActions, Session, Credentials } from '~app/store/session';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '~app/store/store';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
	selector: 'lc-login',
	templateUrl: 'login.component.html',
	styleUrls: [ 'login.component.css' ]
})
export class LoginComponent implements OnInit, OnDestroy {
	sessionSubscription: Subscription;
	formErrorMessage: string = '';
	isPending: boolean = false;

	constructor(
		private ngRedux: NgRedux<AppState>,
		private sessionActions: SessionActions,
		private router: Router
	) {}

	handleLoginBoxSubmit(event: Credentials): void {
		this.sessionActions.login(event);
		this.isPending = true;
		this.formErrorMessage = '';
	}

	ngOnInit(): void {
		this.sessionSubscription = this.ngRedux.select('session')
			.subscribe((session: Session) => {
				if (session.error) {
					this.formErrorMessage = session.error;
				} else if (session.isAuthenticated) {
					this.router.navigate(['/']);
				}

				this.isPending = false;
			});
	}

	ngOnDestroy(): void {
		this.sessionSubscription.unsubscribe();
	}
}
