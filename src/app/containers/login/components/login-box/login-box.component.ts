import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { PASSWORD_MINIMUM_LENGTH } from '~app/validators/password/password.guards';
import { FormValidators } from '~app/validators/form.validators';
import { Credentials } from '~app/store/session';


const ERROR_MESSAGES: any = {
	password: {
		required: 'Please provide your password',
		passwordPolicy: 'Invalid password',
	},
	username: {
		required: 'Please provide your username',
		email: 'Invalid email'
	}
};

@Component({
	selector: 'lc-login-box',
	templateUrl: 'login-box.component.html',
	styleUrls: [ 'login-box.component.css' ]
})
export class LoginBoxComponent implements OnInit {
	loginForm: FormGroup;
	isSubmitted: boolean = false;

	@Output() onSubmit: EventEmitter<Credentials> = new EventEmitter<Credentials>();

	constructor(
		private formBuilder: FormBuilder
	) {}

	ngOnInit(): void {
		this.loginForm = this.formBuilder.group({
			username: ['', [ Validators.required, FormValidators.email() ] ],
			password: ['', [ Validators.required, FormValidators.passwordPolicy() ] ],
			remember: ['']
		});
	}

	handleSubmit(event: Event): void {
		event.preventDefault();

		if (this.loginForm.valid) {
			this.isSubmitted = true;
			this.onSubmit.emit(this.loginForm.value);
		} else {
			Object.keys(this.loginForm.controls).forEach((controlName: string) => {
				this.loginForm.get(controlName).markAsTouched();
				this.loginForm.get(controlName).markAsDirty();
			});
		}
	}

	isControlValid(controlName: string): boolean {
		const control: AbstractControl = this.loginForm.get(controlName);

		if (this.isSubmitted) {
			return (control.valid === true);
		}

		return control.untouched || control.pristine || control.valid;
	}

	getErrorMessageForControl(controlName: string): string {
		let control: AbstractControl = this.loginForm.get(controlName);
		let firstError: string|null = control.errors ? Object.keys(control.errors)[0] : null;

		if (firstError) {
			return ERROR_MESSAGES[controlName][firstError];
		}

		return '';
	}

}
