import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { LoginBoxComponent } from '~app/containers/login/components/login-box/login-box.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Credentials } from '~app/store/session';

function mockEvent(): Event {
	return <Event>{
		preventDefault: () => {}
	};
}

describe('LoginBoxComponent', () => {
	let fixture: ComponentFixture<LoginBoxComponent>;
	let instance: LoginBoxComponent;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
				ReactiveFormsModule
			],
			declarations: [
				LoginBoxComponent
			],
		});

		TestBed.compileComponents().then(() => {
			fixture = TestBed.createComponent(LoginBoxComponent);
			instance = fixture.debugElement.componentInstance;
			instance.onSubmit = jasmine.createSpyObj<EventEmitter<Credentials>>('EventEmitter', ['emit']);
		});
	}));

	it('#handleSubmit method emit onSubmit event and set isSubmitted flag if form was valid', () => {
		instance.loginForm = <any>{
			valid: true,
			value: 'asd'
		};

		instance.handleSubmit(mockEvent());

		expect(instance.onSubmit.emit).toHaveBeenCalledWith('asd');
		expect(instance.isSubmitted).toBe(true);
	});

	it('#handleSubmit method should mark all controls as touched & dirty if form was not valid', () => {
		instance.loginForm = <any>{
			valid: false,
			get: function (name: string) {
				return this.controls[name];
			},
			controls: {
				control1: jasmine.createSpyObj('Control1', ['markAsTouched', 'markAsDirty'] ),
				control2: jasmine.createSpyObj('Control2', ['markAsTouched', 'markAsDirty'] )
			}
		};

		instance.handleSubmit(mockEvent());

		Object.keys(instance.loginForm.controls).forEach((controlName: string) => {
			expect(instance.loginForm.controls[controlName].markAsTouched).toHaveBeenCalled();
			expect(instance.loginForm.controls[controlName].markAsDirty).toHaveBeenCalled();
		});
	});
});
