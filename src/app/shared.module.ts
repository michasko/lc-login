import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoaderComponent } from '~app/components/loader/loader.component';

@NgModule({
	declarations: [
		LoaderComponent
	],
	imports: [
		BrowserModule
	],
	exports: [
		LoaderComponent
	],
	providers: [],
	bootstrap: []
})
export class SharedModule {}
